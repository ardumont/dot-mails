dot-mails
============

My email setup configuration.

This uses:
- offlineimap: sync mailbox from email accounts
- notmuch:     search through indexed emails
- afew:        tag emails

# Install

- clone this repository locally
- cd repository
- ./deploy.sh

# Then what?

Simply adapt the configuration files according to your needs:
- .offlineimaprc:
- afew/config: how to tag emails

# What's missing from that repository

A ~/.authinfo.gpg file holding the credentials information:

This is a text file with the following format:

``` text
machine imap.gmail.com login <login> port <port> password <pass>
```

It's then encrypted using your public key.

# What does this do?

This installs the needed configuration files as symlinks so that the
different tools mentioned work:

/home/$user/.offlineimaprc                           -> /path/to/dot-mails/.offlineimaprc
/home/$user/.notmuch-config                          -> /path/to/dot-mails/dot-mails/.notmuch-config
/home/$user/.config/offlineimap                      -> /path/to/dot-mails/.config/offlineimap
/home/$user/.config/afew                             -> /path/to/dot-mails/.config/afew
/home/$user/.config/systemd/user/offlineimap.timer   -> /path/to/dot-mails/.config/systemd/user/offlineimap.timer
/home/$user/.config/systemd/user/offlineimap.service -> /path/to/dot-mails/.config/systemd/user/offlineimap.timer

Note:
- $user: your login
- /path/to/: local path chosen for cloning the repository

## Example

``` sh
$ ls -lh ~/.offlineimaprc ~/.notmuch-config* ~/.config/{offlineimap,afew} ~/.config/systemd/user
lrwxrwxrwx 1 tony tony   45 Nov  1 14:44 /home/tony/.config/afew -> /home/tony/repo/public/dot-mails/.config/afew
lrwxrwxrwx 1 tony tony   52 Nov  1 14:44 /home/tony/.config/offlineimap -> /home/tony/repo/public/dot-mails/.config/offlineimap
lrwxrwxrwx 1 tony tony   48 Nov  1 14:44 /home/tony/.notmuch-config -> /home/tony/repo/public/dot-mails/.notmuch-config
lrwxrwxrwx 1 tony tony   47 Nov  1 14:44 /home/tony/.offlineimaprc -> /home/tony/repo/public/dot-mails/.offlineimaprc

/home/tony/.config/systemd/user:
total 20K
...
lrwxrwxrwx 1 tony tony   73 Nov  1 14:44 offlineimap.service -> /home/tony/repo/public/dot-mails/.config/systemd/user/offlineimap.service
lrwxrwxrwx 1 tony tony   71 Nov  1 14:44 offlineimap.timer -> /home/tony/repo/public/dot-mails/.config/systemd/user/offlineimap.timer
...
