#!/usr/bin/env bash

CONFIG_DIRS=".config/afew .config/offlineimap"

for dir in $CONFIG_DIRS; do
    ln -nsf $(pwd)/$dir ~/$dir
done

FILES=".notmuch-config .offlineimaprc .signature .signature2"

for file in $FILES; do
    ln -nsf $(pwd)/$file ~/$file
done

SYSTEMD_UNIT='offlineimap.service offlineimap.timer'

CONFIG_DIR=.config/systemd/user
mkdir -p ~/$CONFIG_DIR
for unit in $SYSTEMD_UNIT; do
    ln -nsf $(pwd)/$CONFIG_DIR/$unit ~/$CONFIG_DIR/$unit
done

# since we installed user systemd units
systemctl --user daemon-reload
